﻿namespace LMS.Common.TypeMapping
{
    public interface IAutoMapperTypeConfigurator
    {
        void Configure();
    }
}