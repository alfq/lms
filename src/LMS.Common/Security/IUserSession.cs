﻿namespace LMS.Common.Security
{
    public interface IUserSession
    {
        string Firstname { get; }
        string Lastname { get; }
        string Email { get; }
        bool IsInRole (string roleName);
    }
}