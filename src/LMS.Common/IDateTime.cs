﻿using System;

namespace LMS.Common
{
    public interface IDateTime
    {
        DateTime UtcNow { get; }
    }
}