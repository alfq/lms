using LMS.Data.Entities;

namespace LMS.Data.SqlServer.Mapping
{
    public class BorrowMap : VersionedClassMap<Borrow>
    {
        public BorrowMap()
        {
            Id(x => x.BorrowId);
            References(x => x.Book).Not.Nullable();
            References(x => x.User).Not.Nullable();
            Map(x => x.DateBorrowed).Not.Nullable();
            Map(x => x.DateReturned).Not.Nullable();

        }
    }
}