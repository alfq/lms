﻿using LMS.Data.Entities;

namespace LMS.Data.SqlServer.Mapping
{
    public class BookMap : VersionedClassMap<Book>
    {
        public BookMap()
        {
            Id(x => x.BookId);
            Map(x => x.Title).Not.Nullable();
            Map(x => x.Subject).Not.Nullable();
            Map(x => x.Author).Not.Nullable();
            Map(x => x.Publisher).Not.Nullable();
            Map(x => x.Edition).Not.Nullable();
            Map(x => x.TotalNumberOfBorrowedBooks).Not.Nullable();
            Map(x => x.TotalNumberOfBooks).Not.Nullable();
            Map(x => x.IsAvailable).Not.Nullable();
        }
    }
}