﻿using FluentNHibernate.Mapping;
using LMS.Data.Entities;

namespace LMS.Data.SqlServer
{
    public class VersionedClassMap<T> : ClassMap<T> where T : IVersionedEntity
    {
        protected VersionedClassMap()
        {
            Version(x => x.Version)
                .Column("ts")
                .CustomSqlType("Rowversion")
                .Generated.Always()
                .UnsavedValue("null");
        }
    }
}