﻿using LMS.Data.Entities;
using LMS.Data.QueryProcessors;
using NHibernate;

namespace LMS.Data.SqlServer.QueryProcessor
{
    public class UpdateBookQueryProcessor : IUpdateBookQueryProcessor
    {
        private readonly ISession _session;

        public UpdateBookQueryProcessor(ISession session)
        {
            _session = session;
        }

        public void UpdateBook(long bookId, Book newBook)
        {
            var book = _session.QueryOver<Book>().Where(x => x.BookId == bookId).SingleOrDefault();
            book = newBook;
            _session.SaveOrUpdate(book);

        }
    }
}