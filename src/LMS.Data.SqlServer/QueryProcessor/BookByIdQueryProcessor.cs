﻿using LMS.Data.Entities;
using LMS.Data.QueryProcessors;
using NHibernate;

namespace LMS.Data.SqlServer.QueryProcessor
{
    public class BookByIdQueryProcessor : IBookByIdQueryProcessor
    {
        private readonly ISession _session;

        public BookByIdQueryProcessor(ISession session)
        {
            _session = session;
        }


        public Book GetBook(long bookId)
        {
            var book = _session.Get<Book>(bookId);
            return book;
        }
    }
}