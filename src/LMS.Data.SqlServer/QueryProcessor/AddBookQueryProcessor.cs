﻿using LMS.Data.Entities;
using LMS.Data.QueryProcessors;
using NHibernate;

namespace LMS.Data.SqlServer.QueryProcessor
{
    public class AddBookQueryProcessor : IAddBookQueryProcessor
    {
        private readonly ISession _session;

        public AddBookQueryProcessor(ISession session)
        {
            _session = session;
        }

        public void AddBook(Book book)
        {
            _session.Save(book);
        }
    }
}