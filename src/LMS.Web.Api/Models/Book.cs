﻿using System.Collections.Generic;

namespace LMS.Web.Api.Models
{
    public class Book : ILinkContaining
    {
        private List<Link> _links;
        private bool _shouldSerializeTotalNumberOfBooks;
        private bool _shouldSerializeTotalNumberOfBorrowedBooks;


        public long BookId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Author { get; set; }
        public virtual string Publisher { get; set; }
        public virtual string Edition { get; set; }
        public virtual int TotalNumberOfBooks { get; set; }
        public virtual int TotalNumberOfBorrowedBooks { get; set; }
        public virtual bool IsAvailable { get; set; }
        public int AvailableNumberOfBooks => TotalNumberOfBooks - TotalNumberOfBorrowedBooks;


        public List<Link> Links
        {
            get => _links ?? (_links = new List<Link>());
            set => _links = value;
        }

        public void AddLink(Link link)
        {
            Links.Add(link);
        }

        public void SetShouldSerializeTotalNumberOfBooks(bool shouldSerialize)
        {
            _shouldSerializeTotalNumberOfBooks = shouldSerialize;
        }

        public bool ShouldSerializeTotalNumberOfBooks()
        {
            return _shouldSerializeTotalNumberOfBooks;
        }

        public void SetShouldSerializeTotalNumberOfBorrowedBooks(bool shouldSerialize)
        {
            _shouldSerializeTotalNumberOfBorrowedBooks = shouldSerialize;
        }

        public bool ShouldSerializeTotalNumberOfBorrowedBooks()
        {
            return _shouldSerializeTotalNumberOfBorrowedBooks;
        }
    }
}