﻿using System;

namespace LMS.Web.Api.Models
{
    public class Borrow
    {
//        public long BorrowId { get; set; }
        public virtual Data.Entities.Book Book { get; set; }
        public virtual Data.Entities.User User { get; set; }
        public virtual DateTime DateBorrowed { get; set; }
        public virtual DateTime DateReturned { get; set; }
    }
}