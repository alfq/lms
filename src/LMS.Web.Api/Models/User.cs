﻿namespace LMS.Web.Api.Models
{
    public class User
    {
//        private List<Book> _books;
//        public long UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Username => Lastname + " " + Firstname;

//        public List<Book> Books
//        {
//            get => _books ?? (_books = new List<Book>());
//            set => _books = value;
//        }

//        public void AddBook(Book book)
//        {
//            _books.Add(book);
//        }
    }
}