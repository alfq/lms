﻿using LMS.Common.Logging;
using LMS.Common.TypeMapping;
using LMS.Web.Common;
using System.Web.Http;
using LMS.Common.Security;
using LMS.Web.Api.Security;

namespace LMS.Web.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            RegisterHandlers();

            new AutoMapperConfigurator()
                .Configure(WebContainerManager.GetAll<IAutoMapperTypeConfigurator>());
        }

        private void RegisterHandlers()
        {
            var logManager = WebContainerManager.Get<ILogManager>();
            var userSession = WebContainerManager.Get<IUserSession>();

            GlobalConfiguration.Configuration.MessageHandlers.Add(
                new BasicAuthenticationMessageHandler(logManager,WebContainerManager.Get<IBasicSecurityService>()));

            GlobalConfiguration.Configuration.MessageHandlers.Add(
                new BookDataSerializationMessageHandler(logManager,userSession));
        }

        protected void Application_Error()
        {
            var exception = Server.GetLastError();
            if (exception == null) return;

            var log = WebContainerManager.Get<ILogManager>().GetLog(typeof(WebApiApplication));
            log.Error("Unhandled exception.", exception);
        }
    }
}
