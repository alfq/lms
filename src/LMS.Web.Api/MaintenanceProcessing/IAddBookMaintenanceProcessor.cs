﻿using LMS.Web.Api.Models;

namespace LMS.Web.Api.MaintenanceProcessing
{
    public interface IAddBookMaintenanceProcessor
    {
        Book AddBook(Book newBook);
    }
}