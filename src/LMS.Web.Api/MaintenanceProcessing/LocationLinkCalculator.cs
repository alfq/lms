﻿using System;
using System.Linq;
using LMS.Common;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.MaintenanceProcessing
{
    public class LocationLinkCalculator
    {
        public static Uri GetLocationLink(ILinkContaining linkContaining)
        {
            var locationLink = linkContaining.Links
                .FirstOrDefault(x => x.Rel == Constants.CommonLinkRelValues.Self);
            return locationLink == null ? null : new Uri(locationLink.Href);
        }
    }
}