﻿using LMS.Web.Api.Models;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace LMS.Web.Api.MaintenanceProcessing
{
    public class BookCreatedActionResult : IHttpActionResult
    {
        private readonly Book _createdBook;
        private readonly HttpRequestMessage _requestMessage;

        public BookCreatedActionResult(HttpRequestMessage requestMessage, Book createdBook)
        {
            _requestMessage = requestMessage;
            _createdBook = createdBook;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            var responseMessage = _requestMessage
                .CreateResponse(HttpStatusCode.Created, _createdBook);
            responseMessage.Headers.Location = LocationLinkCalculator.GetLocationLink(_createdBook);
            return responseMessage;
        }
    }
}