﻿using LMS.Common.TypeMapping;
using LMS.Data.QueryProcessors;
using Book = LMS.Web.Api.Models.Book;

namespace LMS.Web.Api.MaintenanceProcessing
{
    public class AddBookMaintenanceProcessor : IAddBookMaintenanceProcessor
    {
        private readonly IAutoMapper _autoMapper;
        private readonly IAddBookQueryProcessor _queryProcessor;

        public AddBookMaintenanceProcessor(IAddBookQueryProcessor queryProcessor, IAutoMapper autoMapper)
        {
            _queryProcessor = queryProcessor;
            _autoMapper = autoMapper;
        }

        public Book AddBook(Book newBook)
        {
            var bookEntity = _autoMapper.Map<Data.Entities.Book>(newBook);
            _queryProcessor.AddBook(bookEntity);

            var book = _autoMapper.Map<Book>(bookEntity);

            //Testing Link

//            book.AddLink(new Link
//            {
//                Method = HttpMethod.Get.Method,
//                Href = "http://localhost:59866/api/v1/books/" + book.BookId,
//                Rel = Constants.CommonLinkRelValues.Self
//            });
            return book;
        }
    }
}