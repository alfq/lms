﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using LMS.Common;
using LMS.Common.Logging;
using LMS.Common.Security;
using LMS.Common.TypeMapping;
using LMS.Data.QueryProcessors;
using LMS.Data.SqlServer.Mapping;
using LMS.Data.SqlServer.QueryProcessor;
using LMS.Web.Api.AutoMappingConfiguration;
using LMS.Web.Api.InquiryProcessor;
using LMS.Web.Api.MaintenanceProcessing;
using LMS.Web.Api.Security;
using LMS.Web.Common;
using log4net.Config;
using NHibernate;
using NHibernate.Context;
using Ninject;
using Ninject.Activation;
using Ninject.Web.Common;

namespace LMS.Web.Api
{
    public class NinjectConfigurator
    {
        public void Configure(IKernel container)
        {
            AddBindings(container);
        }

        private void AddBindings(IKernel container)
        {
            ConfigureLog4Net(container);
            ConfigureUserSession(container);
            ConfigureNHibernate(container);
            ConfigureAutoMapper(container);
            container.Bind<IDateTime>().To<DateTimeAdapter>().InSingletonScope();
            container.Bind<IAddBookQueryProcessor>().To<AddBookQueryProcessor>().InRequestScope();
            container.Bind<IAddBookMaintenanceProcessor>().To<AddBookMaintenanceProcessor>().InRequestScope();
            container.Bind<IBasicSecurityService>().To<BasicSecurityService>().InSingletonScope();
            container.Bind<IBookByIdQueryProcessor>().To<BookByIdQueryProcessor>().InRequestScope();
            container.Bind<IUpdateBookQueryProcessor>().To<UpdateBookQueryProcessor>().InRequestScope();
            container.Bind<IBookByIdInquiryProcessor>().To<BookByIdInquiryProcessor>().InRequestScope();
        }

        private void ConfigureLog4Net(IKernel container)
        {
            XmlConfigurator.Configure();

            var logManager = new LogManagerAdapter();
            container.Bind<ILogManager>().ToConstant(logManager);
        }

        private void ConfigureNHibernate(IKernel container)
        {
            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(
                    c => c.FromConnectionStringWithKey("LMS")))
                .CurrentSessionContext("web")
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<BookMap>())
                .BuildSessionFactory();

            container.Bind<ISessionFactory>().ToConstant(sessionFactory);
            container.Bind<ISession>().ToMethod(CreateSession).InRequestScope();
            container.Bind<IActionTransactionHelper>().To<ActionTransactionHelper>().InRequestScope();
        }

        private ISession CreateSession(IContext context)
        {
            var sessionFactory = context.Kernel.Get<ISessionFactory>();
            if (!CurrentSessionContext.HasBind(sessionFactory))
            {
                var session = sessionFactory.OpenSession();
                CurrentSessionContext.Bind(session);
            }
            return sessionFactory.GetCurrentSession();
        }

        private void ConfigureUserSession(IKernel container)
        {
            var userSession = new UserSession();
            container.Bind<IUserSession>().ToConstant(userSession).InSingletonScope();
            container.Bind<IWebUserSession>().ToConstant(userSession).InSingletonScope();
        }

        private void ConfigureAutoMapper(IKernel container)
        {
            container.Bind<IAutoMapper>().To<AutoMapperAdapter>().InSingletonScope();

            container.Bind<IAutoMapperTypeConfigurator>()
                .To<EntityBookToModelBookAutoMapperTypeConfigurator>()
                .InSingletonScope();
            container.Bind<IAutoMapperTypeConfigurator>()
                .To<ModelBookToEntityBookAutoMapperTypeConfigurator>()
                .InSingletonScope();
            container.Bind<IAutoMapperTypeConfigurator>()
                .To<EntityUserToModelUserAutoMapperTypeConfigurator>()
                .InSingletonScope();
            container.Bind<IAutoMapperTypeConfigurator>()
                .To<ModelUserToEntityUserAutoMapperTypeConfigurator>()
                .InSingletonScope();
            container.Bind<IAutoMapperTypeConfigurator>()
                .To<EntityBorrowToModelBorrowAutoMapperTypeConfigurator>()
                .InSingletonScope();
            container.Bind<IAutoMapperTypeConfigurator>()
                .To<ModelBorrowToEntityBorrowAutoMapperTypeConfigurator>()
                .InSingletonScope();
        }
    }
}