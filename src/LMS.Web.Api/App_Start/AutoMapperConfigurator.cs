﻿using AutoMapper;
using LMS.Common.TypeMapping;
using System.Collections.Generic;
using System.Linq;

namespace LMS.Web.Api
{
    public class AutoMapperConfigurator
    {
        public void Configure(IEnumerable<IAutoMapperTypeConfigurator> autoMapperTypeConfigurators)
        {
            autoMapperTypeConfigurators.ToList().ForEach(x=>x.Configure());
            Mapper.AssertConfigurationIsValid();
        }
    }
}