﻿using LMS.Web.Api.Models;

namespace LMS.Web.Api.InquiryProcessor
{
    public interface IBookByIdInquiryProcessor
    {
        Book GetBook(long bookId);
    }
}