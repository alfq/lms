﻿using LMS.Common.TypeMapping;
using LMS.Data.Exceptions;
using LMS.Data.QueryProcessors;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.InquiryProcessor
{
    public class BookByIdInquiryProcessor : IBookByIdInquiryProcessor
    {
        private readonly IAutoMapper _autoMapper;
        private readonly IBookByIdQueryProcessor _queryProcessor;

        public BookByIdInquiryProcessor(IBookByIdQueryProcessor queryProcessor, IAutoMapper autoMapper)
        {
            _queryProcessor = queryProcessor;
            _autoMapper = autoMapper;
        }

        public Book GetBook(long bookId)
        {
            var bookEntity = _queryProcessor.GetBook(bookId);
            if (bookEntity == null)
            {
                throw new RootObjectNotFoundException("Book not found");
            }
            var book = _autoMapper.Map<Book>(bookEntity);
            return book;
        }
    }
}