﻿using System.Net.Http;
using System.Web.Http;
using LMS.Common;
using LMS.Web.Api.InquiryProcessor;
using LMS.Web.Api.MaintenanceProcessing;
using LMS.Web.Api.Models;
using LMS.Web.Common;
using LMS.Web.Common.Routing;
using LMS.Web.Common.Security;

namespace LMS.Web.Api.Controllers.V1
{
    [ApiVersion1RoutePrefix("books")]
    [UnitOfWorkActionFilter]
    public class BooksController : ApiController
    {
        private readonly IAddBookMaintenanceProcessor _addBookMaintenanceProcessor;
        private readonly IBookByIdInquiryProcessor _bookByIdInquiryProcessor;

        public BooksController(IAddBookMaintenanceProcessor addBookMaintenanceProcessor,
            IBookByIdInquiryProcessor bookByIdInquiryProcessor)
        {
            _addBookMaintenanceProcessor = addBookMaintenanceProcessor;
            _bookByIdInquiryProcessor = bookByIdInquiryProcessor;
        }

        [Route("", Name = "AddBookRoute")]
        [HttpPost]
        [UserAudit]
        [Authorize(Roles = Constants.RoleNames.SeniorLibarian)]
        public IHttpActionResult AddBook(HttpRequestMessage requestMessage, Book newBook)
        {
            var book = _addBookMaintenanceProcessor.AddBook(newBook);
            var result = new BookCreatedActionResult(requestMessage, book);
            return result;
        }

        [Route("{bookId:long}", Name = "GetBookRoute")]
        [Authorize(Roles = Constants.RoleNames.Customer)]
        public Book GetBook(long bookId)
        {
            var book = _bookByIdInquiryProcessor.GetBook(bookId);
            return book;
        }
    }
}
