﻿using LMS.Common;
using LMS.Common.Logging;
using LMS.Common.Security;
using LMS.Web.Api.Models;
using log4net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Web.Api.Security
{
    public class BookDataSerializationMessageHandler : DelegatingHandler
    {
        private readonly ILog _log;
        private readonly IUserSession _userSession;

        public BookDataSerializationMessageHandler(ILogManager logManager, IUserSession userSession)
        {
            _log = logManager.GetLog(typeof(BookDataSerializationMessageHandler));
            _userSession = userSession;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            if (CanHandleResponse(response))
            {
                ApplySecurityToResponseData((ObjectContent) response.Content);
            }
            return response;
        }

        public bool CanHandleResponse(HttpResponseMessage response)
        {
            var objectContent = response.Content as ObjectContent;
            var canHandleResponse = objectContent != null && objectContent.ObjectType == typeof(Book);
            return canHandleResponse;
        }


        public void ApplySecurityToResponseData(ObjectContent responseObjectContent)
        {
            var removeSensitiveData = !_userSession.IsInRole(Constants.RoleNames.SeniorLibarian);
            if (removeSensitiveData)
            {
                _log.DebugFormat("Applying security data masking for user {0}", _userSession.Email);
            }

            ((Book)responseObjectContent.Value).SetShouldSerializeTotalNumberOfBooks(!removeSensitiveData);
            ((Book)responseObjectContent.Value).SetShouldSerializeTotalNumberOfBorrowedBooks(!removeSensitiveData);
        }
    }
}