namespace LMS.Web.Api.Security
{
    public interface IBasicSecurityService
    {
        bool SetPrincipal (string email, string password);
    }
}