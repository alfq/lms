﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using log4net;
using LMS.Common;
using LMS.Common.Logging;
using LMS.Data.Entities;
using LMS.Web.Common;
using NHibernate;

namespace LMS.Web.Api.Security
{
    public class BasicSecurityService : IBasicSecurityService
    {
        private readonly ILog _log;

        public BasicSecurityService(ILogManager logManager)
        {
            _log = logManager.GetLog(typeof(BasicSecurityService));
        }

        public virtual ISession Session => WebContainerManager.Get<ISession>();

        public bool SetPrincipal(string email, string password)
        {
            var user = GetUser(email);
            IPrincipal principal = null;
            if (user == null || (principal = GetPrincipal(user)) == null )
            {
                _log.DebugFormat("System could not validate user {0}", email);
                return false;
            }
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
            return true;
        }

        public virtual IPrincipal GetPrincipal(User user)
        {
            var identity = new GenericIdentity(user.Email, Constants.SchemeTypes.Basic);

            identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Firstname));
            identity.AddClaim(new Claim(ClaimTypes.Surname, user.Lastname));

            var email = user.Email.ToLowerInvariant();
            switch (email)
            {
                case "bjoe@lib.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.Manager));
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.SeniorLibarian));
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.Customer));
                    break;
                case "jbosco@lib.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role,Constants.RoleNames.SeniorLibarian));
                    identity.AddClaim(new Claim(ClaimTypes.Role,Constants.RoleNames.Customer));
                    break;
                case "jdoe@lib.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.Customer));
                    break;
                default:
                    identity.AddClaim(new Claim(ClaimTypes.Role,Constants.RoleNames.Customer));
                    break;
            }
            return new ClaimsPrincipal(identity);
        }

        public virtual User GetUser(string email)
        {
            email = email.ToLowerInvariant();
            return Session.Query<User>().SingleOrDefault(x => x.Email == email);
        }
    }
}