﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class ModelUserToEntityUserAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<User,Data.Entities.User>()
                .ForMember(x=>x.UserId, b=>b.Ignore())
                .ForMember(x => x.Version, b => b.Ignore());
        }
    }
}