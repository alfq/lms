﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class EntityBorrowToModelBorrowAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Data.Entities.Borrow, Borrow>();
        }
    }
}