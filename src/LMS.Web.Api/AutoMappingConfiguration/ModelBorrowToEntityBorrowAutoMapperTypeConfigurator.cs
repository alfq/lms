﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class ModelBorrowToEntityBorrowAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Borrow,Data.Entities.Borrow>()
                .ForMember(x=>x.BorrowId, b=>b.Ignore())
                .ForMember(x=>x.Version, b=>b.Ignore());
        }
    }
}