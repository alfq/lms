﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class EntityBookToModelBookAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Data.Entities.Book, Book>()
                .ForMember(x => x.AvailableNumberOfBooks, b => b.Ignore())
                .ForMember(x=>x.Links, b=>b.Ignore());
        }
    }
}