﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class EntityUserToModelUserAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Data.Entities.User, User>()
                .ForMember(x=>x.Username, u=>u.Ignore());
        }
    }
}