﻿using AutoMapper;
using LMS.Common.TypeMapping;
using LMS.Web.Api.Models;

namespace LMS.Web.Api.AutoMappingConfiguration
{
    public class ModelBookToEntityBookAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Book, Data.Entities.Book>()
                .ForMember(x=>x.BookId, b=>b.Ignore())
                .ForMember(x => x.Version, b => b.Ignore());
        }
    }
}