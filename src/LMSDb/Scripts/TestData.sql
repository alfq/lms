﻿declare @userId int,
		@bookId int,
		@borrowId int

if not exists (select * from [User] where Email = 'bjoe@lib.com')
			INSERT into [dbo].[User] ([Firstname], [Lastname], [Email])
				VALUES (N'Billy', N'Joe', N'bjoe@lib.com')

if not exists (select * from [User] where Email = 'jbosco@lib.com')
			INSERT into [dbo].[User] ([Firstname], [Lastname], [Email])
				VALUES (N'John', N'Bosco', N'jbosco@lib.com')

if not exists (select * from [User] where Email = 'jdoe@lib.com')
			INSERT into [dbo].[User] ([Firstname], [Lastname], [Email])
				VALUES (N'James', N'Doe', N'jdoe@lib.com')


if not exists (select * from [Book] where Title ='Kofi and Ama')
			INSERT into [dbo].[Book] ([Title],[Subject], [Author], [Publisher], [Edition], [TotalNumberOfBooks], [TotalNumberOfBorrowedBooks],[IsAvailable])
				VALUES (N'Kofi and Ama',N'Story', N'Kojo Mensah',N'Apress', N'V1', 20, 5, 1)

if not exists (select * from [Book] where Title ='Animals and Plants')
			INSERT into [dbo].[Book] ([Title],[Subject], [Author], [Publisher], [Edition], [TotalNumberOfBooks], [TotalNumberOfBorrowedBooks],[IsAvailable])
				VALUES (N'Animals and Plants',N'Biology', N'Sulley Musa',N'DanBooks', N'V1', 25, 8, 1)

if not exists (select * from [Book] where Title ='Force and Gravity')
			INSERT into [dbo].[Book] ([Title], [Subject], [Author], [Publisher], [Edition], [TotalNumberOfBooks], [TotalNumberOfBorrowedBooks],[IsAvailable])
				VALUES (N'Force and Gravity',N'Physics', N'Nii Mensah',N'SonkaBooks', N'V1', 30, 7, 1)

if not exists (select * from [Book] where Title ='Chemical Elements')
			INSERT into [dbo].[Book] ([Title],[Subject], [Author], [Publisher], [Edition], [TotalNumberOfBooks], [TotalNumberOfBorrowedBooks],[IsAvailable])
				VALUES (N'Chemical Elements',N'Chemistry', N'Sowah Amos',N'AppleBooks', N'V1', 15, 2, 1)



if not exists (select * from dbo.Borrow)
begin

	select top 1 @bookId = BookId from Book order by BookId;
	select top 1 @userId = UserId from [User] order by UserId;

	insert into dbo.Borrow ( BookId, [UserId] , DateBorrowed, DateReturned)
		values(@bookId , @userId , GETDATE(), GETDATE())

	set @borrowId = SCOPE_IDENTITY();


end