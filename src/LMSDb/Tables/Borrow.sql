﻿CREATE TABLE [dbo].[Borrow] (
	[BorrowId] BIGINT IDENTITY(1,1) NOT NULL,
	[BookId] BIGINT NOT NULL,
	[UserId] BIGINT NOT NULL,
	[DateBorrowed] DATETIME2 (7) NOT NULL,
	[DateReturned] DATETIME2 (7) NULL,
	[ts] rowversion NOT NULL
	foreign key (BookId) references dbo.[Book] (BookId),
	foreign key (UserId) references dbo.[User] (UserId)

)

go 
create index ix_BookUser_UserId on Borrow(UserId)
go