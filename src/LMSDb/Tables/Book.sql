﻿CREATE TABLE [dbo].[Book] (
	[BookId] BIGINT IDENTITY (1,1) NOT NULL,
	[Title] [nvarchar](80) NOT NULL,
	[Subject] [nvarchar] (80) NOT NULL,
	[Author] [nvarchar] (80) NOT NULL,
	[Publisher] [nvarchar] (80) NULL,
	[Edition] [nvarchar] (50) NOT NULL,
	[TotalNumberOfBooks] BIGINT NOT NULL,
	[TotalNumberOfBorrowedBooks] BIGINT NULL,
	[IsAvailable] BIT NOT NULL,
	[ts] rowversion NOT NULL
	PRIMARY KEY CLUSTERED ([BookId] ASC)

)