﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using log4net;
using LMS.Common.Logging;
using LMS.Common.Security;

namespace LMS.Web.Common.Security
{
    public class UserAuditAttribute : ActionFilterAttribute
    {
        private readonly ILog _log;
        private readonly IUserSession _userSession;

        public UserAuditAttribute() : this(WebContainerManager.Get<ILogManager>(),
            WebContainerManager.Get<IUserSession>())
        {

        }

        public UserAuditAttribute(ILogManager logManager, IUserSession userSession)
        {
            _log = logManager.GetLog(typeof(UserAuditAttribute));
            _userSession = userSession;
        }

        public override bool AllowMultiple => false;

        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            _log.Debug("Starting execution...");
            var email = _userSession.Email;
            return Task.Run(()=> AuditCurrentUser(email), cancellationToken);
        }

        public void AuditCurrentUser(string email)
        {
            //Simulate long auditing process
            _log.InfoFormat("Action being executed by user = {0}", email);
            Thread.Sleep(3000);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            _log.InfoFormat("Action executed by user = {0}", _userSession.Email);
        }
    }
}