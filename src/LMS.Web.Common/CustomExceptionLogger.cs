﻿using LMS.Common.Logging;
using log4net;
using System.Web.Http.ExceptionHandling;

namespace LMS.Web.Common
{
    public class CustomExceptionLogger : ExceptionLogger
    {
        private readonly ILog _log;

        public CustomExceptionLogger(ILogManager logManager)
        {
            _log = logManager.GetLog(typeof(CustomExceptionLogger));
        }

        public override void Log(ExceptionLoggerContext context)
        {
            _log.Error("Unhandled exception", context.Exception);
        }
    }
}