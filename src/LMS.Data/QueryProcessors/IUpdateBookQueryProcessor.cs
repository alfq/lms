﻿using LMS.Data.Entities;

namespace LMS.Data.QueryProcessors
{
    public interface IUpdateBookQueryProcessor
    {
        void UpdateBook(long bookId, Book newBook);
    }
}