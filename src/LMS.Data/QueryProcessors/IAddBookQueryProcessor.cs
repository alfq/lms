﻿using LMS.Data.Entities;

namespace LMS.Data.QueryProcessors
{
    public interface IAddBookQueryProcessor
    {
        void AddBook(Book book);
    }
}