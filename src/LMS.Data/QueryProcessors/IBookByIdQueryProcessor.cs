﻿using LMS.Data.Entities;

namespace LMS.Data.QueryProcessors
{
    public interface IBookByIdQueryProcessor
    {
        Book GetBook(long bookId);
    }
}