﻿using System;

namespace LMS.Data.Exceptions
{
    [Serializable]
    public class ChildObjectNotFoundException : Exception
    {
        
    }
}