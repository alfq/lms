﻿using System;

namespace LMS.Data.Exceptions
{
    [Serializable]
    public class RootObjectNotFoundException : Exception
    {
        public RootObjectNotFoundException(string message) : base (message) 
        {
            
        }
    }
}