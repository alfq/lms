﻿namespace LMS.Data.Entities
{
    public interface IVersionedEntity
    {
        byte[] Version { get; set; }
    }
}