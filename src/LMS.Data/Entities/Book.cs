namespace LMS.Data.Entities
{
    public class Book : IVersionedEntity
    {
        public virtual long BookId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Author { get; set; }
        public virtual string Publisher { get; set; }
        public virtual string Edition { get; set; }
        public virtual int TotalNumberOfBooks { get; set; }
        public virtual int TotalNumberOfBorrowedBooks { get; set; }
        public virtual bool IsAvailable { get; set; }
        public virtual byte[] Version { get; set; }
    }
}