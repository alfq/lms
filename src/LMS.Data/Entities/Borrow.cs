using System;

namespace LMS.Data.Entities
{
    public class Borrow : IVersionedEntity
    {
        public virtual long BorrowId { get; set; }
        public virtual Book Book { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime? DateBorrowed { get; set; }
        public virtual DateTime? DateReturned { get; set; }
        public virtual byte[] Version { get; set; }
    }
}